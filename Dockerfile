FROM node:12-alpine
COPY ./app /app
WORKDIR /app
RUN npm i
EXPOSE 8000
CMD ["npm", "start"]
